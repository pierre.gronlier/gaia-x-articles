\documentclass{article}
\usepackage[a4paper, total={7in, 9in}]{geometry}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{hyperref}
\usepackage{amsmath}
\usepackage{centernot}
\usepackage{graphicx}
\usepackage{multicol}
\usepackage{comment}
\usepackage{array}

\usepackage[
backend=biber,
style=alphabetic,
sorting=ynt
]{biblatex}

\addbibresource{references.bib}

\date{\small \today\\v1.7}

\title{An introduction to the Gaia-X Trust Framework}

\author{
\small Pierre Gronlier \\
%\small GAIA-X European Association for Data and Cloud
}

\begin{document}

\maketitle

%% Add the abstract in English
\begin{abstract}
Gaia-X has for mission to:
\begin{center}
\begin{tabular}{ | m{15cm} | }
\hline
"Enable trusted decentralised digital ecosystems creating the \textit{de facto} standard aligned with European values by developing a set of policies, rules, specifications and a verification framework." \\
\hline
\end{tabular}
\end{center}
To achieve the above mission a unique and new framework was developed by associating state-of-the-art and cutting-edge market standards: the Gaia-X Trust Framework.
\end{abstract}

\begin{multicols}{2}

\section{Introduction}

The Gaia-X Trust Framework is designed with three objectives in mind:

\begin{itemize}
    \item help the users of the framework to make educated decisions, independently of the jurisdictions and domains they are operating in.
    \item be extendable for specific jurisdictions (Japan, \dots) and domains needs (finance, health, \dots)
    \item be the starting point for the elaboration of automated compliance.
\end{itemize}

To be noted that this document does assume knowledge of concepts like vocabularies, cryptographic certificates and data structures that are common to most ICT\footnote{ICT : Information and Communication Technology}  engineering fields and is referring to external resources the reader should be familiar with.

The first important element of that framework is Trust. In accordance with the commonly accepted English definitions \footnote{trust: \href{https://www.collinsdictionary.com/dictionary/english/trust}{Collins}, \href{https://dictionary.cambridge.org/dictionary/english/trust}{Cambridge}, \href{https://www.oxfordlearnersdictionaries.com/definition/english/trust}{Oxford}}  for trust, trust is defined in this document as "\textit{the favourable response of a decision-making party who assesses the risk concerning the target party's ability to fulfil a promise}".

This definition implies the following:
\begin{itemize}
    \item Trust involves at minima two parties: the decision-making party $p_1$ and the target party $p_2$. 
    \item Trust is not automatically reciprocal or mutual: the fact that the decision-making party trusts the targeted party doesn’t imply that the reverse is true:
    $p_1 \text{ trusts } p_2 \centernot \implies p_2 \text{ trusts } p_1$.
    \item Trusted $(1)$ or untrusted $(0)$ is the result of the decision-making process $T_\chi(p_1, p_2)$ performed by the decision-making party $p_1$ on a promise $\chi$ and using a risk threshold $\tau_\chi$ as a decision criterion.

    It’s important to note that the risk assessment is rarely an abrupt binary decision; risk is usually expressed by a combination of heuristic\footnote{heuristic: \url{https://en.wikipedia.org/wiki/Heuristic}} $r_n \circ \dots \circ r_1$ in terms of risk sources\footnote{risk source: \url{https://www.iso.org/obp/ui/en/\#iso:std:iso:31000:ed-2:v1:en:term:3.4}}, potential events \footnote{potential event: \url{https://www.iso.org/obp/ui/en/\#iso:std:iso:31000:ed-2:v1:en:term:3.5}}, their consequences \footnote{consequence: \url{https://www.iso.org/obp/ui/en/\#iso:std:iso:31000:ed-2:v1:en:term:3.6}} and their likelihood \footnote{likelihood: \url{https://www.iso.org/obp/ui/en/\#iso:std:iso:31000:ed-2:v1:en:term:3.7}}.
    $$T_\chi(p_1,p_2) = \begin{cases}
    \text{1} & \text{if } r_n \circ \dots \circ r_1 (p_1,p_2, \chi) > \tau_\chi \\
    \text{0} & \text{else}
    \end{cases}$$
    This also means that the same decision-making party $p_1$, having access to the same available information about target party’s $p_2$ capabilities, might make different final trust decisions, depending on the promises $\chi_1, \chi_2, \dots$.
\end{itemize}

The Gaia-X Trust framework gives a methodology and technical specifications for collecting and organising the information to perform this risk assessment.

Later in this document, a party $p \in P$ \footnote{W3C Open Digital Rights Language party: \url{https://www.w3.org/TR/odrl-model/\#party}}, which can be either a $P = \{$ legal entity, natural person, process\footnote{process: \url{https://en.wikipedia.org/wiki/Process_(computing)}} $\}$, is always uniquely identifiable,  $\forall p_1,p_2 \in P, \text{if } p_1 \ne p_2 \text{ then } Id(p_1) \ne Id(p_2)$.

To conclude this introduction, the Gaia-X Trust framework helps to objectivise and rationalise the risk assessment and decision-making process and can be used to implement automated compliance.

The second important element of that framework is Interoperability. The European Interoperability Framework\footnote{European Interoperability Framework: \url{https://ec.europa.eu/isa2/eif_en/}} is listing four different interoperability layers: Legal, Organisational, Semantic, Technical.

The Gaia-X Trust Framework is targeting the Organisational and Semantic layers by providing an ontology\footnote{ontology: \url{https://en.wikipedia.org/wiki/Ontology_(information_science)}} and a set of second order logic\footnote{$2^{nd}$ order logic: \url{https://en.wikipedia.org/wiki/Second-order_logic}} rules to translate the European values of transparency, openness, self-determination, privacy and interoperability into machine actionable information.

While the Gaia-X Trust Framework is primarily targeting ICT services and data products, the technical protocols and data formats used by the Gaia-X framework can easily be reused and adapted for other use cases such as the EU Digital Product Passport\cite{EU_DPP} for batteries, cosmetics, clothes, \dots

\section{Fundamentals}

For the decision-making party to perform a risk assessment, information, referred later as claims, about the target party, its capabilities, and the promise, referred later as the objects of the assessments, must be collected.

To organise the collected claims, the Gaia-X Trust Framework relies on asymmetric cryptography\footnote{public-key: \url{https://en.wikipedia.org/wiki/Public-key_cryptography}} and linked data\footnote{W3C Linked-Data: \url{https://www.w3.org/DesignIssues/LinkedData.html}} to:
\begin{itemize}
    \item build a machine-readable knowledge graph of claims about the objects of assessment.
    \item be able to verify at any time the content integrity of the claims.
    \item keep track from where the claims originated from or to keep track of the parties issuing the claims, referred later as the issuers.
\end{itemize}

Then, to have organisational and semantic interoperability, the Gaia-X Trust Framework provides:
\begin{itemize}
    \item generic information models such as the Licensor-Licensee,  Producer-Provider or Provider-Consumer relations, the law of obligations \footnote{law of obligations: \url{https://en.wikipedia.org/wiki/Law_of_obligations}}, the contracting procedures, and rules.
    \item data exchange specific information models such as data transactions, data intermediaries, consent management for data processing, policy management.
    \item vocabularies and schemata to describe the characteristics of the objects of the assessments, and enable policy reasoning on those characteristics\cite{10.1007/978-3-030-17294-7_4} \cite{10.1007/978-3-030-31095-0_3}, like ICT services, cloud offerings like big data cluster, AI training and inferencing engines, hardware and software infrastructure including edge devices, data product, the licence and terms of uses.
\end{itemize}

To be noted that the last point introduces the topic of automated or supervised policy reasoning, which is a key element for future smart legal contract\cite{EU_Smart_Contract} \cite{SmartLegalContracts}, including legally binding contracts\cite{Contract_Law}.


This framework also enables both parties $p_1$ and $p_2$ to exchange their roles where $p_2$ becomes the decision-making party and $p_1$ the target party.
For example, in a context where $p_2$ provides data processing services, $p_1$ might want to ensure that the services provided by $p_2$ meet its needs in terms of legal, operational and technical autonomy and $p_2$ might want to ensure that $p_1$ will use the service and process the data in accordance with the agreed terms of service and data processing purposes.


To ease and foster the adoption by the market of the Gaia-X Trust Framework, the Gaia-X Association members decided to reuse as much as possible to existing vocabularies and terms defined in:

\begin{itemize}
    \item The W3C Verifiable Credentials\footnote{W3C Verifiable Credential: \url{https://w3c.github.io/vc-data-model/}} and Linked-Data \footnote{linked-data: \url{https://www.w3.org/wiki/LinkedData}} standards.
    \item The vocabulary and general principles from the ISO Committee on Conformity Assessment (ISO/CASCO)\footnote{ISO/CASCO: \url{https://www.iso.org/obp/ui/en/\#iso:std:iso-iec:17000:ed-2:v2:en}}
\end{itemize}

\section{Guidelines for the Gaia-X policy-rules makers}

To help a decision-making party with its assessment, the Gaia-X policy-rules makers, ie Gaia-X Association members, have predefined four assessments scheme:

\begin{itemize}
    \item The Gaia-X Conformity which specifies the mandatory requirements \footnote{requirement: \url{https://www.iso.org/obp/ui/\#iso:std:iso-iec:17000:ed-2:v2:en:term:5.1}} for an object to be qualified as Gaia-X compliant.
    \item The Gaia-X Label level 1, level 2 and level 3 which add extra requirements specifically to EEA\footnote{EEA: European Economic Area} legislation.
\end{itemize}

In the context of Gaia-X globalisation and keeping Gaia-X core EU values intact, it’s important to note that it’s up to the Gaia-X members to be innovative and articulate Gaia-X requirements that capture the intent of the legislation without necessarily referring to it.

For example, given the following two requirements with similar objectives:
\begin{itemize}
    \item "\textit{Regardless of its location and the location of the service users, a service provider must comply with EU GDPR\footnote{\href{https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX\%3A32016R0679}{General Data Protection Regulation}}.}"
	\item "\textit{If personal or sensitive data is processed by a service provider, the later must always be able to prove, on request, that the owner of the data has given unequivocal consent for its processing and for explicit purposes and duration.}"
\end{itemize}

The first one adds little value to EEA providers and EEA residents, because the GDPR is already enforced by law. However it adds a significant burden on providers not initially subject to EU law and surely slows down the adoption of Gaia-X standards globally where similar data protection regulations are already in place: Japan\footnote{\href{https://www.cas.go.jp/jp/seisaku/hourei/data/APPI.pdf}{The Act on the Protection of Personal Information Act No. 57 of 2003}}, Brazil\footnote{\href{http://www.planalto.gov.br/ccivil_03/_ato2015-2018/2018/lei/l13709.htm}{General Personal Data Protection Law}}, Singapore\footnote{\href{https://sso.agc.gov.sg/Act/PDPA2012}{Personal Data Protection Act 2012}} , USA/California\footnote{\href{https://oag.ca.gov/privacy/ccpa}{California Consumer Privacy Act}} , USA/Virginia\footnote{\href{https://lis.virginia.gov/cgi-bin/legp604.exe?212+ful+CHAP0036+pdf}{Virginia Consumer Data Protection Act}} , \dots

The second one offers Gaia-X policy-rules makers the opportunity to further detail the semantics and syntax of the claims to be collected to demonstrate compliance with the requirement, such as consent, processing purposes, duration, service provider contact point for information requests, revocation of consent and so on, to reach semantic interoperability across existing data protection regulations.

This extra effort by Gaia-X members to detail as much as possible the semantics and syntax of the information required to provide evidence that a requirement is fulfilled is directly proportional to the future effectiveness of organisational and semantic interoperability.

To help the Gaia-X policy-rules makers, a decision flow chart\ref{fig:criteria-workflow}, has been developed in accordance with the ISO/CASCO and W3C Verifiable Credentials standards, starting from a high-level criterion down to measurable and comparable requirements.

This workflow is summarised as follows:
\begin{enumerate}
    \item For each requirement, the Gaia-X policy-rules makers must decide if the claims must be validated or verified.
    \begin{itemize}
        \item For a validation\footnote{validation: \url{https://www.iso.org/obp/ui/\#iso:std:iso-iec:17000:ed-2:v2:en:term:6.5}} , a $1^{st}$ or $2^{nd}$ party assessment activity is sufficient.
        \item For verification\footnote{verification: \url{https://www.iso.org/obp/ui/\#iso:std:iso-iec:17000:ed-2:v2:en:term:6.6}}, a $3^{rd}$ party assessment activity is required.
    \end{itemize}
    \item For each claim, the Gaia-X policy-rules makers must specify one or more of the following lists:
    \begin{itemize}
        \item The accepted claim’s issuers.
        \item The characteristics qualifying an accepted issuer.
        \item The characteristics qualifying an accepted public-key to digitaly sign the claim.
    \end{itemize}
\end{enumerate}

Those lists or the results of the evaluation of the characteristics from those lists are referred in the Gaia-X Trust Framework as Trust Anchors and are published via the Gaia-X Registry service.

The careful selection of the Trust Anchors is critical for the result of the assessment to be legally relevant or legally binding.

Finally, a special attention from the Gaia-X policy-rules makers must be given to the scope\footnote{scope: \url{https://www.iso.org/obp/ui/\#iso:std:iso-iec:17000:ed-2:v2:en:term:7.4}} of the collected claims.

For each requirement $r$, the union of the scopes $s(c_i)$ of the collected claims $c_i$ must cover the entire scope $s(r)$ of the requirement $r$, such as $s(r) = s(r) \cap \Big( \bigcup_i^ns(c_i) \Big)$

\section{Guidelines for the Gaia-X policy-rules users}

As described in the previous section, Gaia-X policy-rules makers have defined four assessment scheme:

\begin{itemize}
    \item Gaia-X Conformity: the provider and consumer can make informed and educated decisions based on information gathered to demonstrate European values.
    \item Gaia-X Label level 1: on top of the Gaia-X Conformity, the provider and consumer can rely on their mutual declaration of adherence to the European data protection rules.
    \item Gaia-X Label level 2: on top of the Gaia-X Label level 1, cybersecurity criteria have been verified by impartial third parties and data can be processed exclusively in the European Economic Area.
    \item Gaia-X Label level 3: on top of the Gaia-X Label level 2, the data is processed exclusively in the European Economic Area and cannot be accessed by parties from outside the EEA.
\end{itemize}

This categorisation gives the users of the framework a mean to apply for an assessment and/or order services and data products with a rank $r$ on a scale from 0 to 4 – non-compliant to compliant with Gaia-X Label level 3 - by increment of 1. $r \in \{ 0,1,2,3,4 \}$.

However, this ranking:
\begin{itemize}
    \item Doesn’t automatically adapt to the market which always evolves faster than the rules.
    \item Doesn’t capture the subtleties of organisational and semantic interoperability complexity, \textit{de facto} lowering the interoperability to the basic commonly conceded denominators.
\end{itemize}

To address this challenge, additional scoring tools named Trust Indexes are developed as Veracity $T_V$, Transparency $T_T$, Composability $T_C$ and Semantic-Match $T_{SM}$ indexes. Those indexes enable the users of the Gaia-X Trust Framework:
\begin{itemize}
    \item As a consumer to compare objects or offerings with a more granular ranking that wouldn’t necessarily fit into one of the predefined conformity schemes.
    
Eg: A service offering compliant with Label level 1 but not Label level 2 will be assessed with a score $r_{SO}$  in the range $2.0$ to $3.0$ excluded, as in $r_{SO} \in [ 2.0, 3.0[ $
    \item The providers to compare their offering with existing ones and help them to improve their interoperability; "\textit{As a provider, is my service interoperable and composable with 10\% or 90\% of the other offerings in the Gaia-X Catalogues ?}"
 \end{itemize}

The proposed Trust Indexes $T_V,T_T,T_C,T_{SM}$ are meant to be used by all parties - licensor, licensee, producer, provider, consumer, \dots - as a measure of distance for interoperability and trust with regards to the other offerings in the Gaia-X Catalogues; the intent being that Gaia-X helps the market by providing measurement tools and let the market actors themselves to converge to an optimum solution, similar to gradient descent algorithms\footnote{Gradient descent: \url{https://en.wikipedia.org/wiki/Gradient_descent}} where an error metric is given and a process iteratively applied converge to an optimum solution.

For example, a provider that declares its services or products by providing only the bare minimum information without machine readable claims nor policies may still meet Gaia-X Compliance criteria but will have a low interoperability score with other offerings from the Gaia-X Catalogues and it would be in the provider’s own interest to improve the quality and quantity of the provided information.

The Trust Indexes will be described more in detail in a separate document.
%scoring formula requires to deep-dive into asymmetric cryptographic keychain, RDF embeddings\footnote{Example of RDF embedding: \url{http://rdf2vec.org/}}, ontologies and will be done in a future separate document.

\end{multicols}

\section{Conclusion}

The Gaia-X Trust Framework specification is the result of a multi-disciplinary work astride the domains of compliance, maths, ICT and data privacy regulations with the mission to help organisational and semantic interoperability.

Furthermore, this framework describes an intention, a methodology, an architecture, and rules which, combined, are greater than the sum of the elements analysed separately, which give this framework a \textit{sui generis} quality.

This framework will be further developed as an enabler for automated compliance and a separate document will be published to describe possible deployment models.

\printbibliography

\newpage

\newgeometry{top=10mm, bottom=10mm, left=10mm, right=10mm}

\section*{Annexe 1}

\begin{figure*}[!ht]
    \centering
    \includegraphics[angle=90,width=0.8\linewidth]{workflow.png}
    \caption{Workflow for building a criteria}
    \label{fig:criteria-workflow}
\end{figure*}

\restoregeometry 


\end{document}
