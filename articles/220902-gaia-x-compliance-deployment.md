# Gaia-X Compliance Service deployment scenario

The Gaia-X Association is a new and still young initiative, which celebrated its first year of activity. During the last 12 months, our members’ community grew from a 22 to a 350-member base.

The sum of all Gaia-X deliverables and how they relate to each other is now visually depicted and described in the Gaia-X Framework at <https://docs.gaia-x.eu/framework>.

The Gaia-X Framework will continuously evolve – make sure to bookmark and visit it regularly!

Our pace is influenced by two main factors: the urgent need, for Europe and beyond, of a new generation of trustworthy digital services, controllable by the data subjects and data controllers, and the commitment of our members to deliver this into the market.

To enable this change, a common governance framework for the free and safe flow of data in the digital space is required, similarly to how Europe adopted a common governance for its border control, free flow of goods, common currency, and many more.

At Gaia-X, we are developing a digital governance based on regulatory, industry standards and requirements from our members contributing to the project (large and small industries, providers and users of technologies, academics, and trade association) to let them define the rules and characteristics necessary to fulfil the real market expectations.

What makes us different is that we are not only defining these rules in written specifications, but we are also implementing it into a technology framework – i.e. source code – to achieve what we call **Gaia-X Compliance**.

The Gaia-X Compliance is split in two main subsystems:

- The '**Trust framework**' – which is mandatory and verifies the existence and veracity of any service characteristics, and,
- The '**Labelling framework**' – which is optional and allows to verify adherence to rule-sets that fulfil specific market needs.

The first 'Trust Framework' subsystem enforces a common set of rules every participant and services in the Gaia-X ecosystem must be verifiable against to be **Gaia-X Compliant**.

The aim is to **measure and compare** the legal, technical, and operational **autonomy levels** of those services through transparency on the service composability, on the service characteristics, on the claims of compliance to existing standards, on the portability and interoperability capabilities – covering licenses, workloads, and data.

🧑‍💻 Dev-to-dev: the 'Trust Framework' is our operational answer to the Trust plane described in the NIST 500-332 Cloud Federation Reference Architecture. It’s based on Linked Data stored in W3C Verifiable Credentials documents. Each Verifiable Credential can contain several types of claim and is signed using a JSON Web Key. Each type of claim is defined by a specific controlled vocabulary and a list of Gaia-X approved issuers. Having intermediate keys is possible.

The list of eligible issuers public keys and schema are available via the Gaia-X Registry service, a sub-component of the Gaia-X Compliance service.

The second 'Labelling framework' subsystem enables anyone – for instance, a sovereign government to enforce the guidelines, or a trade association – to define their common rules for their sector.

This **extensibility**, leveraging the existing Trust Framework, allows any specific federation of participants to add their own specifics rules to increase trust and security across their participants.

🧑‍💻 Dev-to-dev: the 'Labelling Framework' enables federations to extend the Gaia-X controlled vocabularies; restrict the list of approved claim issuers and apply additional conformity logic rules.

As mentioned above, the **Gaia-X Compliance** is at the heart of the Gaia-X value and our approach to keep its adoption easy, effort and time effective is to implement it as a software. Previously referred as "regulation by automation", this is evolving into the next phase of "**Compliance as Code**" – a subject to be covered in a future article.

To operationalise the Gaia-X Compliance, its software implementations must be executed and turned into an up and running services, providing traceable evidence of corrected executions.

While the governance of the Gaia-X Compliance rules and process is and will stay under the control of the Gaia-X association, the Gaia-X Compliance services will go through several mutually non-exclusive deployments scenario.

## Today

In short, we pragmatically need to start somewhere! What Gaia-X has today is a simple central deployment of the compliance code in container.

This option enables us to quickly deploy our first software version with very low management effort and minimal cost. Users of the Compliance service are mainly our first Gaia-X Lighthouse projects – a solution proven to fit our current scaling requirements.

However, this is not the final landing model neither an acceptable deployment scenario for the long run, as it puts the responsibility of the service on the Association itself; create a singularity and cannot scale in a geo-distributed environment with heterogenous network transport layers.

🧑‍💻 Dev-to-dev: Current deployment is a Service in a single Kubernetes multi-node managed cluster deployed from a CI/CD pipeline.

## Future

We envision the following models for the time being:

### The licenced model:

To keep a tight control, the Gaia-X association licenses a release of the Gaia-X Compliance source code to several identified Gaia-X members.

The association could also create a testbed to validate 3rd party implementation of the Gaia-X Compliance. This option is not envisioned at this time.

This model requires contractual legal binding agreements between the Gaia-X Association and every selected Gaia-X member to cover liability, RTO, MTD, SLA, updates/upgrades, maintenance, and operations.

It offers better scalability and resilience than the current one. However, it is still not optimal in terms of transparency and openness, leaving the control of the Gaia-X Compliance in the hands of specific market subjects. In addition, it will force the Gaia-X Association to manage and supervise all contracts.

### The private decentralised model:

The evolution of the previous model with an improved scalability and better identification of potential rule breakers, would be the deployment of a Gaia-X Consortium blockchain to execute the Gaia-X Compliance service.

This model is more controllable, has fast transaction speed and would open a playground for tokenomics.

However, this only provides privacy of the service and participant descriptions within the scope of the Cconsortium.

### The secure private model

Similar to the previous model with multiple deployment instances, but to eliminate the need for a manual legal binding agreement and enable instances of the service to be executed in environments without prior knowledge on their ownership or their security level, the Gaia-X Association requires specific proofs for the correctness of the execution.

The technologies being evaluated are:

- the Remote Attestation feature from modern CPU Trusted Execution Enclaves (TEE) (e.g.: Intel SGX, AMD SEV).
- Hybrid hardware and software solutions with execution traceability from a trusted bootloader up to workload attestation (examples are CNCF Keylime, Threefold ZeroOS, Istio/SPIRE).

### The public decentralised model:

Despite the general fame of Web3, very few services are managing to stay decentralised. Hence, further study and analysis should be reserved.

This model would run the Gaia-X Compliance software as a smart-contract on one or more public blockchain networks.

At this point in time, this is only an under-evaluation technology with smart-contract in Ethereum Web Assembly – eWASM.

🧑‍💻 Dev-to-dev: Summary

| Model	                      | Ease for version update | Scalability | Privacy control |
|-----------------------------|-------------------------|-------------|-----------------|
| Single instance             |          +++            |    ---      |       ++        |
| Licensed                    |          ++             |    --       |       +++       |
| Private decentralised model |          ++             |     +       |       -         |
| Secure private model        |           +             |    +++      |       -         |
| Public decentralised model  |           -             |    +++      |       ---       |


In conclusion, all the models above have their pros and cons and are not mutually exclusive. All models will have to be technically validated before being submitted for approval to the Gaia-X Technical Committee.

Gaia-X is welcoming contributions, new ideas, technical expertise or technical validation. Please liaise back with Cristina.Pauna@gaia-x.eu to support with relevant feedback.

Join our open-source community today!
