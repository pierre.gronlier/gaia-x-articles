# Sovereignty and Trust (Part 1)

Have you ever wondered how to implement '*data sovereignty*', '*digital sovereignty*', '*sovereign cloud*' and other variations of these terms?

Well, here is a guide to get you as close as possible.

## Replace sovereignty with autonomy

> sovereignty (uncountable noun): Sovereignty is the power that a country has to govern itself or another country or state.  
> https://www.collinsdictionary.com/dictionary/english/sovereignty

And here's the first complication: sovereignty is tied to a country, and therefore to its sovereign 🫅.  
So if you are not a sovereign - i.e. a king, a queen, a president, a tsar, an emperor, ... - the first suggestion is to talk about autonomy.

> autonomy (uncountable noun): Autonomy is the ability to make your own decisions about what to do rather than being influenced by someone else or told what to do.   
> https://www.collinsdictionary.com/dictionary/english/autonomy

There are at least three different level of autonomy that you can customise 🎛️ according to your use case:

- Technical autonomy
- Operational autonomy
- Legal autonomy

### Technical autonomy 🔎

This is your ability to decide and control what data, software and hardware is used.

**Low level of technical autonomy**: you don't know what data, software or hardware is being used.  
There is no Bill of Material (BOM) or Software Bill of Material (SBOM). You are given a black box.

**High level of technical autonomy**: you have full control - supply, production, development, ... - of the data, software or hardware used.  
You have the source code, the architectural plans and the schematics, with their documentation, their version, and the possibility of auditing to assess what is "inside".

### Operational autonomy 🏗️

It's your ability to operate the data, software and hardware.

**Low level of operational autonomy**: you're tied to one operator and can't switch to another. There is no portability.  

**High level of operational autonomy**: you have the resources - configuration files, software and hardware licences - and knowledge to operate the data, software and hardware yourself if you wish. Portability is possible.

### Legal autonomy 🗺️

That's your ability to decide under which jurisdictions and policies the data, software and hardware are licensed and operated.

**Low level of legal autonomy**: You are forced to comply with jurisdictions and policies that may negatively affect your business, competitiveness or privacy.

**High level of legal autonomy**: You have the ability to make informed decisions about which jurisdictions and policies - licences, terms and conditions, ... - to which your data and services are subject.

Note that your jurisdictions and policies depend on your geographical location, the geographical location of your customers, your business domains, but you can choose those of your providers.

## Data 💾 and Services 🧑‍🍳

The previous section introduced the concepts of data and services and in a nutshell:

- data is a digital representation of information.
- a service is a running instance of an algorithm.

To go further, it's important to remember that "data doesn't flow on a rainbow".

That is:

1. Data can only be processed, stored or transferred by using a service, i.e. a running instance of an algorithm.  
For example, no data can replicate or transfer itself without running a service.
2. The algorithm to process, store, transfer the data can be embedded in the data itself, but still requires an external service to interpret and execute that algorithm.  
The Fetch - Decode - Execute cycle is at the core of all our modern CPU architectures.
3. The algorithm to process, store or transfer the data is data itself.
4. The cloud is just someone else's servers.  
There is no 'your cloud' unless you run your own infrastructure on-premises, and in that situation you may still have a low level of technical autonomy.

## Assessing autonomy 🛂

Given the above, to assess your levels of autonomy, you need to ask yourself at least these three questions

1. What is the SBOM and BOM used to process, store, transfer your data ?
2. Who operates the services and hardware?
3. Where are these operators geographically located?

You can, of course, drill down as deep as you need into the answers you get to include dependencies, sub-contractors, ultimate beneficial owners (UBO), ....

## Interoperability ↔️ vs. software lock-in 🔒

This last section is a reminder that interoperability of any kind - technical, semantic, organisational, legal to reuse those from the [EIF](https://ec.europa.eu/isa2/eif_en/) - comes from common specifications, common vocabularies.

If you do a proper autonomy assessment, any proposed solution based on a hegemonic software or hardware solution should raise a warning about the creation of software or hardware vendor lock-in.

Open-source software and hardware, depending of their license and the governance of the project, can offer a raisonable level of technical autonomy.
However, this does not guarantee a level of operational autonomy if the software or hardware, comes with a level of configuration and maintenance complexity that you can't handle.

This is particularly true in new areas, such as data spaces, where business models have yet to prove sustainable.

## Conclusion

Talking about sovereignty is about risk assessment and evaluating your autonomy to enforce your decisions.
