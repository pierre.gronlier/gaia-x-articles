# Gaia-X : Compliance as Code

Following on the previous Gaia-X Compliance article, today’s article will focus on a term introduced a few months ago in Gaia-X, "**Compliance as Code**": what is it, why we need it and how to use it.

Let’s start in January 2000 when Lawrence Lessig published his famous "**Code Is Law**" article.

Lessig article's main idea is that the software, which is literally surrounding us today, is constraining our behaviour. The software – or code - then acts as the "law" because it restricts and draws the boundary of what the users can do or not.

Example: an UI without a button or an API without a route to export personal data would lock a user from migrating its data to another service. The service's developers are "regulating" what the user can do or not.

This paradigm of "code is law" is still true nowadays but slowly shifting towards "law is code" where the regulations define the set of action and draw the boundary of what can and should be done in the digital space.

Examples of such regulations in Europe are, and not limited to, the Digital Service Act, the Digital Market Act, the Data Governance Act, the next Data Act and the future other acts being developed by the European Commission.

The analysis of the regulations is out of scope for this article which, however, will open the discussion of how to operationalise those, especially in the digital space when the regulations are over hundreds of text pages.

The field of Regulatory – Technology (Regtech) already exists for a decade and supports companies to be in conformity with the regulations via the use of technology.

Most of the work done in this area is on the digitalisation of paper-based process and still heavily rely on manual assessment.

The concept of Compliance as Code comes from **Infrastructure as Code** where the managing and provisioning of infrastructure is done through source code enabling **repeatability** and **reproducibility** to enforce **consistency** instead of tedious manual process.

In addition to the features provided by digitalisation: content integrity verification, signature identification, scaling, ... it can leverage all existing tooling used for software development: versioning, release management, testing, ...

The second goal of Compliance as Code is to **produce digital proofs that are legally relevant** and can be assessed by the parties involved in the source code and if needed the judicial authorities of the appropriate jurisdiction.

🧑‍💻Dev-to-dev: the proofs can be, and not limited to, monitoring or history logs, workload remote attestations, cryptographic signatures for integrity and authentication, like the `DKIM-Signature` header for emails.

It is important to note that Compliance as Code does not change the rule of law or governance. It remains a tool that does not claim to replace human judgement and expertise.

🧑‍💻Dev-to-dev: A typical, _CAUTION, this machine has no brain, use your own._ type of disclaimer is needed to avoid `rm -rf /`

## Descriptive models

Like for Infrastructure as Code solutions which define models for deploying networks, compute instances, storage, load balancer, ..., a descriptive model is required for the Compliance as Code.

In Gaia-X context, the main model is defined in the [Gaia-X Architecture document](https://docs.gaia-x.eu/technical-committee/architecture-document/22.04/conceptual_model/).

<img src="https://docs.gaia-x.eu/technical-committee/architecture-document/22.04/figures/GAIA-X_models-Main.png" width="250px">

Additional more detailed descriptive models will complement this one and will be covering:

- Service Composition
- Data Exchange
- Identity and Access management

## Languages

Given a descriptive model, the next step is to create a language.

Below are two examples of Infrastructure as Code to deploy an OpenStack Virtual Machine instance, one using YAML file formatted for Ansible, one using Terraform language HCL.
  
```yaml
- name: new Gaia-X Compliance instance
  openstack.cloud.server:
       state: present
       name: basic
       image: ad091b52-742f-469e-8f3c-fd81cadf0743
       key_name: my_key_pair
       timeout: 200
       flavor: compliance_debian
       security_groups: sg_internal
       nics:
         - net-name: net_internal
       meta:
         role: compliance
```

Ansible code for a Virtual Machine instance

```json
resource "openstack_compute_instance_v2" "basic" {
  name            = "basic"
  image_id        = "ad091b52-742f-469e-8f3c-fd81cadf0743"
  flavor_name     = "compliance_debian"
  key_pair        = "my_key_pair"
  security_groups = ["sg_internal"]
  network {
    name = "net_internal"
  }
  metadata = {
    role = "compliance"
  }
```

Terraform code for a Virtual Machine instance

Reading the code above, even if it's two different languages, it's possible to identify similarities and patterns because their descriptive models are also similar.

Creating a language for a given domain and objectives is not new: Esperanto, Klingon, Dothraki, Quenya, C++, Haskell, Prolog, Python, ... _Ha'_

For conciseness of the article, let us focus on the ones relevant for our topic. We need to

-	Select the writing system
-	Define the syntax or grammar
-	Define the semantic fields

The first point can be addressed by using Unicode characters encoded in UTF-8.

The second point is very much linked to Control Structures for conditional, repetitive or sequential flows and variable assignment.

🧑‍💻Dev-to-dev: if-elseif-else, switch, for, foreach, while, ++i, ...

The third point is the interesting one and can be split into sequential steps

1. Define controlled vocabulary
2. Define taxonomy
2. Define ontology

<svg width="200" height="180">
    <ellipse cx="50%" cy="50%" rx="50%" ry="50%" stroke="blue" stroke-width="2" fill="white" fill-opacity="0.2"/>
    <text x="50%" y="10%" fill="black" stroke-width="2px" dy="1em" text-anchor="middle" alignment-baseline="middle">Controlled vocabulary</text>
    <svg y=10%>
      <ellipse cx="50%" cy="50%" rx="45%" ry="35%" stroke="cyan" stroke-width="2" fill="white" fill-opacity="0.2"/>
      <text x="50%" y="25%" fill="black" stroke-width="2px" dy=".3em" text-anchor="middle" alignment-baseline="middle">Taxonomy</text>
      <svg y=10%>
        <ellipse cx="50%" cy="50%" rx="40%" ry="20%" stroke="purple" stroke-width="2" fill="white" fill-opacity="0.2"/>
        <text x="50%" y="50%" fill="black" stroke-width="2px" dy=".3em" text-anchor="middle" alignment-baseline="middle">Ontology</text>
      </svg>
    </svg>
</svg>


### Controlled vocabularies

Given a domain, defining a controlled vocabulary is to come up with a **flat list of terms** that have a unique unambiguous definition within that domain.

A control vocabulary used by several entities or in Gaia-X context, several ecosystems or dataspaces, enables **semantic interoperability** by providing a consistent representation of information across documents and records.

Semantic interoperability is here an important notion. It's the ability to automatically interpret the information exchanged between two or more parties.

Example: One of the Gaia-X Label criteria is for the provider to define the roles and responsibilities of each party in a contract.  
This criterion is useful to provide transparency and a high added value would be to enable the **comparison** of those roles and responsibilities across different contracts. This is possible only if a shared controlled vocabulary for roles and responsibilities is adopted.

Creating a controlled vocabulary requires domain experts and it's not expected for a controlled vocabulary to always cover all the possible domain scenario and use cases.

🧑‍💻Dev-to-dev:
```javascript
actionsVocab = ["archive", "delete", "display", "extract", "install", "modify", "..."] // from https://www.w3.org/TR/odrl-vocab/
```

However controlled vocabularies are basic construct and don't offer any type of rule inference nor structured extension. For that, we need a taxonomy.

### Taxonomies

A taxonomy is a **hierarchically ordered controlled vocabulary**.

The simplest example in the Gaia-X descriptive model is the class `Participant` and its hyponyms `LegalParticipant` and `NaturalParticipant`.

🧑‍💻Dev-to-dev: WARNING, in semantic subclassing from two classes gives you the intersection, not the union of the two.

With this approach, it's possible to classify and create relation between terms.

Given a Gaia-X defined taxonomy, it's possible for the ecosystems or dataspaces to easily extend it with their own taxonomy, while keeping a common layer of semantic interoperability.

<!-- {
  "theme": "base",
  "themeVariables": {
    "primaryColor": "#CCE4FF",
    "edgeLabelBackground": "#ffffff",
    "tertiaryColor": "#b900ff11",
    "tertiaryTextColor": "#000",
    "tertiaryBorderColor": "#b900ff"
  },
  "themeCSS": ".label, .cluster-label, .actor, .messageText, .noteText, .loopText, .labelText {font-family: 'Titillium Web' !important; font-size: 10pt !important} .cluster-label {font-weight: 700}"
} -->

```mermaid
flowchart BT
subgraph Shared Gaia-X taxonomy
    class1[Class1]
    class2[Class2]
    class11[Subclass1_1]
    class12[Subclass1_2]
    class21[Subclass2_1]
    class22[Subclass2_2]
end
    class11 --> class1
    class12 --> class1

    class21 --> class2
    class22 --> class2

subgraph Domain2 extension
    class211[Subclass2_1_1]
    class212[Subclass2_1_2]
end
subgraph Domain1 extension
    class111[Subclass1_1_1]
end
    class211 --> class21
    class212 --> class21
    class111 --> class11
```

Given one of the main objective of the Gaia-X association to ease and accelerate data exchange, it is of high important to address the need of semantic interoperability accross ecosystems and dataspaces. This is the reason why several Gaia-X Working Groups are defining taxonomies by **identifying the invariants**: it enables to **measure and compare** criteria and compute rules using a common vocabulary.

Warning: the focus here is on the semantic interoperability for the negociation of the service access or data usage, not about the data being exchanged.

It's important to note that defining a taxonomy does not define rules. It is still the **responsability** of the parties involved in the negocation to come up with their own criteria. The parties operating the engine to compute the rules are **accountable** for the execution of the negotiation - this will be covered in a future article about the traceability and integrity of a software execution.

Example: two dataspaces using the same taxonomy to negociate data retention might define different retention periods.

🧑‍💻Dev-to-dev: a REGO example

```go
default allow := false

allow if {
   shortRetention
   locatedInEEA
}

shortRetention if input.DataRetention < data.maxDataRetention

locatedInEEA if input.processingLocation.countryCode == data.EEA[_][_]
```

While the above example gives a glimpse on how negociation can be improved in terms of transparency and effort, a taxonomy does not capture the domain specific regulations and meaning formulated in natural language. For that, we need an ontology.

### Ontologies

An "**ontology is a formal, explicit specification of a shared conceptualisation**". (Gruber,1993)

In our case, it means to extract knowledge from texts in natural language and create a model that is understandable by an algorithm so one can automate the rules of the text with a computer.

Building an ontology is significantly more complex than building a taxonomy and for the time being, we limit ourselves to ontology capturing the invariants per domain and by priority relevant for our members.

🧑‍💻Dev-to-dev: checkout a [small ontology](http://owlgred.lumii.lv/online_visualization/pizza.owl) and a more [complex one](http://owlgred.lumii.lv/online_visualization/pizza2.owl) for a Pizza menu .

The Gaia-X Compliance is an ontology and our first version can be found [here](https://docs.gaia-x.eu/policy-rules-committee/trust-framework/22.04/service/).

Also, even if each domain – Service composition, Data Contract, SLA terms, EULA, Privacy Statements, ... - might start with distinct ontologies, it is expected that those ontologies will cross-reference each others.

🧑‍💻Dev-to-dev: using the built-in [`owl:sameAs`](https://www.w3.org/TR/owl-ref/#sameAs-def) property, it’s possible to indicate that two URI refer to the same `owl:Thing`.

## Usage

Going back to the original topic of Compliance as Code, assuming we now have a suitable language, the next step is to write the specification and develop a software stack enabling technical interoperability, ensuring the traceability and integrity of both the exchanged messages and computed results, **across ecosystems**

Several associations are alreading working together:

- <https://docs.trustrelay.io/elements/dataspaces>
- <https://docs.x-road.global/>
- <https://ishareworks.atlassian.net/wiki/spaces/IS/pages/70222165/Technical>
- <https://github.com/International-Data-Spaces-Association/IDS-RAM_4_0/tree/main/documentation>

🧑‍💻Dev-to-dev: we need to develop the equivalent of the Infrastructure as Code commands below, with **immutable** logs

```shell
$ terraform [plan|apply|destroy]
$ ansible-playbook <playbook.yml>
```

The next article will cover how Verifiable Credentials are used as the backbone of the Gaia-X Compliance and the importance of the Verifiable Data Registry governance.

In conclusion, Compliance as Code is a mean to consistently produce digital proofs that are legally relevant, using semantic interoperability of a shared ontology and a software stack capable of ensuring traceability and integrity of the rules execution.

Given their respective expertise domain, each Gaia-X Group is expected to contribute to the Gaia-X ontology and provide technical expertise or technical validation. Please liaise back with Cristina.Pauna@gaia-x.eu to support with relevant feedback.

Check out https://docs.gaia-x.eu/framework/ to browse our deliverables.

_Qapla' !_



<!-- <svg width="200" height="180">
    <ellipse cx="50%" cy="50%" rx="50%" ry="50%" stroke="blue" stroke-width="2" fill="white" fill-opacity="0.2"/>
    <text x="50%" y="15%" fill="black" stroke-width="2px" dy="1em" text-anchor="middle" alignment-baseline="middle">Gaia-X Trust Anchors</text>
    <svg y=10%>
      <ellipse cx="50%" cy="50%" rx="45%" ry="30%" stroke="cyan" stroke-width="2" fill="white" fill-opacity="0.2"/>
      <text x="50%" y="50%" fill="black" stroke-width="2px" dy=".3em" text-anchor="middle" alignment-baseline="middle">
        <tspan x="50%" dy="0em">Domain specific</tspan>
        <tspan x="50%" dy="1em">Trust Anchors</tspan>
      </text>
    </svg>
</svg>


<svg width="200" height="180">
    <ellipse cx="50%" cy="50%" rx="50%" ry="50%" stroke="purple" stroke-width="2" fill="white" fill-opacity="0.2"/>
    <text x="50%" y="15%" fill="black" stroke-width="2px" dy="1em" text-anchor="middle" alignment-baseline="middle">
        <tspan x="50%" dy="0em">Domain specific</tspan>
        <tspan x="50%" dy="1em">compliance rules</tspan>
    </text>
    <svg y=10%>
      <ellipse cx="50%" cy="50%" rx="45%" ry="30%" stroke="blue" stroke-width="2" fill="white" fill-opacity="0.2"/>
      <text x="50%" y="50%" fill="black" stroke-width="2px" dy=".3em" text-anchor="middle" alignment-baseline="middle">
        <tspan x="50%" dy="0em">Gaia-X</tspan>
        <tspan x="50%" dy="1em">compliance rules</tspan>
      </text>
    </svg>
</svg> -->


<!-- The Gaia-X Label and Gaia-X Compliance are ongoing efforts to translate high level requirements into measurable and comparable criteria in order to enable an objective evaluation of #legal, #technical and #operational #autonomy levels.


The second objective of the Gaia-X Compliance is to produce digital proofs that are legally relevant and can be assessed by the parties involved and if needed the judicial authorities of the appropriate jurisdiction. #gaiax #compliance 


As the Gaia-X CTO, I support member states seeking for more #transparency. -->

<!-- We are in a situation where the compliance and certification state of the art is unsigned PDF when it's not just [scanned paper](https://www.ssi.gouv.fr/uploads/2022_569_np.pdf).
![image](/uploads/29ca0aee7ee4b19f77b70d58b03a0928/image.png)

What gaia-x is doing is not to solve all issues at once but is making a tiny step in the direction of:

"maybe, instead of paying $$$ to obtain a certification based on self-made declaration, admittedly verify by a 3rd party but still based on a self-declared reference document, and providing unverifiable claims to the end-users, could we skip this entire workflow which anyway only the biggest players can afford, and replace it with a list of atomic, measurable and comparable set of legally relevant signed claims"

To achieve the above goal, the standard CAB model doesn't work. See !54. -->
