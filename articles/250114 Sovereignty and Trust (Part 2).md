# Sovereignty and Trust (Part 2)

Have you ever wondered how to implement '*data sovereignty*', '*digital sovereignty*', '*sovereign cloud*' and other variations of these terms?

Well, here is a guide to get you as close as possible.

This article is part of series. The [part 1](./250114%20Sovereignty%20and%20Trust%20(Part%202).md) covered the "sovereignty" part. This one focus on trust, or more specifically trustworhtiness. 

# Trust or Trustworthyness 

Back to the dictionary, trust can't be sold or bought. 

> trust (uncountable noun): Your trust in someone is your belief that they are honest and sincere and will not deliberately do anything to harm you.  
> https://www.collinsdictionary.com/dictionary/english/trust

Whether or not you trust something or someone to act according to your expectation, a promise, is the result of your risk assessment that you performed taking into account:

- the information about the capacity of the party to fullfill the promise
- the repercutions if the promise is not fulfilled. 

In you don't have access to those information, and assess them, you are gambling 🎰.

## information
Some woud like to
