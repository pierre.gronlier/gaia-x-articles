#/bin/sh

set -e

for f in $(find . -name *.ipynb); do
    [ $(echo "$f" | grep '.ipynb_checkpoints') ] && continue
    echo $f
    jupyter nbconvert --ClearOutputPreprocessor.enabled=True --clear-output --ClearMetadataPreprocessor.enabled=True --inplace --log-level=ERROR "$f"
done
