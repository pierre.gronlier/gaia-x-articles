#/bin/sh

set -ex

jupyter lab --ip=0.0.0.0 --LabServerApp.open_browser=False
