import os
import io
from ftplib import FTP
from pyld import jsonld
from jose import jws
import json
from jwcrypto import jwk, jws
from jwcrypto.common import json_encode
from hashlib import sha256
import datetime

def compact_token(token):
    parts = token.split(".")
    return parts[0] + ".." + parts[2]


def save_doc(config, doc):
    name =  os.path.basename(doc['id'])
    print("saving file {} at {}".format(name, doc['id']))
    with FTP(config['host']) as ftp:
        ftp.login(config['user'], config['pass'])
        ftp.cwd(config['root'])
        data = io.BytesIO(str.encode(json.dumps(doc)))
        ftp.storlines("STOR " + name, data)
#        ftp.retrlines('LIST')


# from https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/vc-issuer/-/blob/main/code/app.py
def sign_doc(doc, privkey, issuerKey):
    doc["validFrom"] = datetime.datetime.now(datetime.timezone.utc).replace(microsecond=0).isoformat()
    doc["validUntil"] = (datetime.datetime.now(datetime.timezone.utc) + datetime.timedelta(days=90)).replace(microsecond=0).isoformat()
    normalized = jsonld.normalize(doc, {'algorithm': 'URDNA2015', 'format': 'application/n-quads'})
    hash = sha256(normalized.encode('utf-8'))
    jwstoken = jws.JWS(hash.hexdigest())
    jwstoken.add_signature(privkey, None,
                           json_encode({"alg": "PS256", "b64": False, "crit" :["b64"]}),
                           json_encode({"kid": privkey.thumbprint()}))
    signed = jwstoken.serialize(compact=True)
    doc['proof'] = {
        "type": "JsonWebSignature2020",
        "proofPurpose": "assertionMethod",
        "verificationMethod": issuerKey,
        "jws": compact_token(signed)
    }
    return doc


#from rdflib.extras.external_graph_libs import rdflib_to_networkx_multidigraph
#import networkx as nx
#import matplotlib.pyplot as plt

#def draw_graph(graph):
#    G = rdflib_to_networkx_multidigraph(graph)
#    # Plot Networkx instance of RDF Graph
#    pos = nx.spring_layout(G, scale=2)
#    edge_labels = nx.get_edge_attributes(G, 'r')
#    nx.draw_networkx_edge_labels(G, pos, edge_labels=edge_labels)
#    nx.draw(G, with_labels=True)
